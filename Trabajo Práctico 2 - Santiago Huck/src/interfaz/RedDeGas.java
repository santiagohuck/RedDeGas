package interfaz;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import programa.*;

import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class RedDeGas 
{
	JFrame ventana;
	private Grafo grafo;
	private JLabel labelFlujo;
	private JPanel panelMapa, panelControles;
	private JScrollPane panelAristas, panelNodos;
	private JTable tablaNodos, tablaAristas;
	private JMapViewer mapa;
	
	private JButton btnAgregarGasoducto, btnEliminarGasoducto, btnFlujoMax;
	private JButton btnGuardar, btnCargar, btnNuevaRed;
	
	private ArrayList<Tupla<String,Integer>> listaNodos;
	private ArrayList<Tupla<Integer,Integer>> listaAristas;
	private ArrayList<Integer> listaAristasCap;
	private ArrayList<Tupla<String,Coordinate>> listaNodosMap;
	private ArrayList<MapPolygonImpl> listaAristasMap;
	
	private JLabel lbl1, lbl2, lbl3, lbl4, lbl5, lbl6;
	private JButton btnSalir;
	private JLabel lblRedDeGas;
	
	private String actual;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() {
				try {
					RedDeGas redDeGas = new RedDeGas();
					redDeGas.ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public RedDeGas() 
	{
		inicializar();
	}

	private void inicializar() 
	{				
		iniciarVentana();
		iniciarRed();
		iniciarPaneles();
		iniciarMapa();
		iniciarJScrollPanels();
		iniciarTablas();
		iniciarFlujos();	
		iniciarLabels();
		iniciarBotonesMenu();
		agregarNodo();
		agregarGasoducto();				
		eliminarGasoducto();		
		
		
	}
	
	private void iniciarVentana() {
		ventana = new JFrame();
		ventana.setTitle("Red de Gas");
		ventana.setBounds(100, 100, 1200, 600);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);		
	}
		
	private void iniciarRed() {
		grafo = new Grafo();
		listaNodos = new ArrayList<Tupla<String,Integer>>();
		listaAristas = new ArrayList<Tupla<Integer,Integer>>();
		listaAristasCap = new ArrayList<Integer>();
		listaNodosMap = new ArrayList<Tupla<String,Coordinate>>();
		listaAristasMap = new ArrayList<MapPolygonImpl>();
	}

	private void iniciarPaneles() {
		panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 521, 540);
		ventana.getContentPane().add(panelMapa);		
		panelControles = new JPanel();
		panelControles.setBounds(1009, 11, 165, 425);
		ventana.getContentPane().add(panelControles);		
		panelControles.setLayout(null);
	}
	
	private void iniciarMapa() {
		mapa = new JMapViewer();
		mapa.setBounds(10, 11, 501, 518);
		mapa.setZoomContolsVisible(false);
		mapa.setDisplayPositionByLatLon(-40.521, -63.7008, 4);
		panelMapa.setLayout(null);		
		panelMapa.add(mapa);
	}

	
	private void iniciarJScrollPanels() {
		panelAristas = new JScrollPane();
		panelAristas.setBounds(871, 11, 128, 540);
		ventana.getContentPane().add(panelAristas);
		
		panelNodos = new JScrollPane();
		panelNodos.setBounds(541, 11, 320, 540);
		ventana.getContentPane().add(panelNodos);
	}
	
	private void iniciarFlujos() {
		btnFlujoMax = new JButton("Flujo M\u00E1ximo");
		btnFlujoMax.setBounds(1022, 462, 140, 23);
		ventana.getContentPane().add(btnFlujoMax);
		
		labelFlujo = new JLabel((String) null);
		labelFlujo.setFont(new Font("Calibri", Font.BOLD, 16));
		labelFlujo.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		labelFlujo.setFocusable(false);
		labelFlujo.setHorizontalAlignment(SwingConstants.CENTER);
		labelFlujo.setBounds(1022, 496, 140, 42);
		ventana.getContentPane().add(labelFlujo);
		
		btnFlujoMax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
								
				grafo.prepararFM();				
				FlujoMaximo fm = new FlujoMaximo();
				labelFlujo.setText("Flujo = "+fm.flujoMaximo(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1));
				actualizarTabla();
			}
		});
	}
	
	private void iniciarBotonesMenu() {
		btnGuardar = new JButton("Guardar Red");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarRed();
			}
		});
		btnGuardar.setBounds(10, 274, 145, 23);
		panelControles.add(btnGuardar);
		
		btnCargar = new JButton("Cargar Red");
		btnCargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargarRed();
			}
		});
		btnCargar.setBounds(10, 308, 145, 23);
		panelControles.add(btnCargar);
		
		btnNuevaRed = new JButton("Nueva Red");
		btnNuevaRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nuevaRed();
			}
		});
		btnNuevaRed.setBounds(10, 240, 145, 23);
		panelControles.add(btnNuevaRed);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(10, 379, 145, 23);
		panelControles.add(btnSalir);
	
	}

	private void iniciarLabels() {
		
		lblRedDeGas = new JLabel("Red de Gas");
		lblRedDeGas.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRedDeGas.setHorizontalAlignment(SwingConstants.CENTER);
		lblRedDeGas.setBounds(10, 0, 145, 23);
		panelControles.add(lblRedDeGas);
		
		lbl1 = new JLabel("un nodo en el mapa.");
		lbl1.setBounds(10, 46, 145, 23);
		panelControles.add(lbl1);
		
		lbl2 = new JLabel("-Click Izquierdo: A\u00F1adir");
		lbl2.setBounds(10, 31, 145, 23);
		panelControles.add(lbl2);
		
		lbl3 = new JLabel("-Click Derecho mantenido:");
		lbl3.setBounds(10, 70, 155, 23);
		panelControles.add(lbl3);
		
		lbl4 = new JLabel("Moverse en el mapa.");
		lbl4.setBounds(10, 85, 145, 23);
		panelControles.add(lbl4);
		
		lbl5 = new JLabel("Controlar Zoom del mapa.");
		lbl5.setBounds(10, 124, 155, 23);
		panelControles.add(lbl5);
		
		lbl6 = new JLabel("-Rueda del Mouse: ");
		lbl6.setBounds(10, 109, 155, 23);
		panelControles.add(lbl6);
	}	

	private void iniciarTablas() {
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tablaNodos = new JTable();
		tablaNodos.setEnabled(false);
		tablaNodos.setFocusable(false);
		panelNodos.setViewportView(tablaNodos);
		tablaNodos.setModel(modeloTablaNodos(this.listaNodos));		
		tablaNodos.getColumnModel().getColumn(0).setPreferredWidth(55);
		tablaNodos.getColumnModel().getColumn(1).setPreferredWidth(125);
		tablaNodos.getColumnModel().getColumn(2).setPreferredWidth(70);
		tablaNodos.getColumnModel().getColumn(3).setPreferredWidth(70);
		tablaNodos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		tablaNodos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
		
		tablaAristas = new JTable();
		tablaAristas.setEnabled(false);
		tablaAristas.setFocusable(false);
		panelAristas.setViewportView(tablaAristas);
		tablaAristas.setModel(modeloTablaAristas(listaAristas));
		tablaAristas.getColumnModel().getColumn(0).setPreferredWidth(64);
		tablaAristas.getColumnModel().getColumn(1).setPreferredWidth(64);
		tablaAristas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
	}

	public DefaultTableModel modeloTablaNodos(ArrayList<Tupla<String,Integer>> listaNodos)
	{
		
		DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("Numero");
		tableModel.addColumn("Nombre");
		tableModel.addColumn("Capacidad");
		tableModel.addColumn("Tipo");
		
			    
		for (int i=0; i<listaNodos.size();i++){
			tableModel.addRow(new String[] {listaNodos.get(i).getElem2()+"",listaNodos.get(i).getElem1(),
			grafo.getCapacidades()[listaNodos.get(i).getElem2()][listaNodos.get(i).getElem2()]+"",grafo.getTipos()[listaNodos.get(i).getElem2()][listaNodos.get(i).getElem2()]+""});
		}
		return tableModel;	
		
	}
	
	public DefaultTableModel modeloTablaAristas(ArrayList<Tupla<Integer,Integer>> listaAristas)
	{
		
		DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("Gasoducto");	
		tableModel.addColumn("Capacidad");	
			    
		for (int i=0; i<this.listaAristas.size();i++){
			tableModel.addRow(new String[] {listaAristas.get(i).getElem1()+"  ---->  "+listaAristas.get(i).getElem2()+"",listaAristasCap.get(i)+""});
		}
		return tableModel;	
		
	}
	
	public boolean esInteger(String string) {
	    try {
	        Integer.valueOf(string);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
	
	private void agregarNodo() 
	{				
		mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
			if (e.getButton() == MouseEvent.BUTTON1)
			{
			    Coordinate markeradd = mapa.getPosition(e.getPoint());			    
				String nombre = JOptionPane.showInputDialog("Nombre: ");
				String tipo = JOptionPane.showInputDialog("Tipo: ");
				if (!tipo.equals("Productor")&&!tipo.equals("Consumidor")&&!tipo.equals("De Paso")){
					JOptionPane.showMessageDialog(ventana, "El tipo debe ser 'Productor', 'Consumidor' o 'De Paso', no '" + tipo+"'.");
					throw new IllegalArgumentException("El tipo debe ser 'Productor', 'Consumidor' o 'De Paso', no '" + tipo+"'.");
				}
				String capacidad = JOptionPane.showInputDialog("Capacidad: ");
				if (esInteger(capacidad)==false || Integer.parseInt(capacidad)<0){
					JOptionPane.showMessageDialog(ventana, "La capacidad deber ser un numero entero positivo, no '" + capacidad+"'.");
					throw new NumberFormatException("La capacidad deber ser un numero entero positivo, no '" + capacidad+"'.");
				}				
				listaNodosMap.add(new Tupla<String,Coordinate>(nombre, markeradd));
				grafo.agregarVertice(tipo, Integer.parseInt(capacidad));
				listaNodos.add(new Tupla<String,Integer>(nombre,grafo.vertices()-1));				
				actualizarTabla();
				mapa.addMapMarker(new MapMarkerDot(nombre, markeradd));
				
				
			}}
		});
	}
	
	private void agregarGasoducto() 
	{
		btnAgregarGasoducto = new JButton("Agregar Gasoducto");
		btnAgregarGasoducto.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				String origen = JOptionPane.showInputDialog("Nodo de Origen: ");
				if (Integer.parseInt(origen)>grafo.vertices()-1 || Integer.parseInt(origen)<0){
					JOptionPane.showMessageDialog(ventana, "El nodo '" + origen+"' no pertenece a la red.");
					throw new IllegalArgumentException("El nodo '" + origen+"' no pertenece a la red.");
				}
				
				String destino = JOptionPane.showInputDialog("Nodo de Destino: ");
				if (Integer.parseInt(destino)>grafo.vertices()-1 || Integer.parseInt(destino)<0){
					JOptionPane.showMessageDialog(ventana, "El nodo '" + destino+"' no pertenece a la red.");
					throw new IllegalArgumentException("El nodo '" + destino+"' no pertenece a la red.");
				}
				
				String capacidad = JOptionPane.showInputDialog("Capacidad: ");
				if (esInteger(capacidad)==false || Integer.parseInt(capacidad)<0){
					JOptionPane.showMessageDialog(ventana, "La capacidad no puede ser negativa");
					throw new IllegalArgumentException("La capacidad no puede ser negativa");
				}
				
				if (grafo.existeArista(Integer.parseInt(origen), Integer.parseInt(destino))){
					JOptionPane.showMessageDialog(ventana, "El gasoducto ya existe en la red");
					throw new IllegalArgumentException("El gasoducto ya existe en la red");
				}
				
				grafo.agregarArista(Integer.parseInt(origen), Integer.parseInt(destino), Integer.parseInt(capacidad));
				listaAristas.add(new Tupla<Integer,Integer>(Integer.parseInt(origen),Integer.parseInt(destino)));
				listaAristasCap.add(Integer.parseInt(capacidad));
				
				MapPolygonImpl camino = new MapPolygonImpl(listaNodosMap.get(Integer.parseInt(origen)).getElem2(), 
				listaNodosMap.get(Integer.parseInt(destino)).getElem2(),listaNodosMap.get(Integer.parseInt(origen)).getElem2());
				listaAristasMap.add(camino);
				mapa.addMapPolygon(camino);
				
				actualizarTabla();
				actualizarMapa();
				
			}
		});
		btnAgregarGasoducto.setBounds(10, 158, 145, 23);
		panelControles.add(btnAgregarGasoducto);
	}

	private void eliminarGasoducto() 
	{
		btnEliminarGasoducto = new JButton("Eliminar Gasoducto");
		btnEliminarGasoducto.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				
				String origen = JOptionPane.showInputDialog("Nodo de Origen: ");
				String destino = JOptionPane.showInputDialog("Nodo de Destino: ");
				
				grafo.eliminarArista(Integer.parseInt(origen), Integer.parseInt(destino));
				for (int i=0;i<listaAristas.size();i++){
					if (listaAristas.get(i).getElem1()==Integer.parseInt(origen) && listaAristas.get(i).getElem2()==Integer.parseInt(destino)){
						listaAristas.remove(i);
						listaAristasCap.remove(i);
						listaAristasMap.remove(i);
					}
				}
				mapa.removeAllMapPolygons();
				actualizarTabla();		
				actualizarMapa();
				
				
			}
		});
		btnEliminarGasoducto.setBounds(10, 193, 145, 23);
		panelControles.add(btnEliminarGasoducto);
	}
	
	public void actualizarTabla()
	{
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tablaNodos.setModel(modeloTablaNodos(listaNodos));
		tablaNodos.getColumnModel().getColumn(0).setPreferredWidth(55);
		tablaNodos.getColumnModel().getColumn(1).setPreferredWidth(125);
		tablaNodos.getColumnModel().getColumn(2).setPreferredWidth(70);
		tablaNodos.getColumnModel().getColumn(3).setPreferredWidth(70);
		tablaNodos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		tablaNodos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
		tablaAristas.setModel(modeloTablaAristas(listaAristas));
		
	}
	
	public void actualizarMapa(){
		mapa.removeAllMapPolygons();
		for (int i = 0;i<this.listaNodosMap.size();i++){
			this.mapa.addMapMarker(new MapMarkerDot(listaNodosMap.get(i).getElem1(),listaNodosMap.get(i).getElem2()));
		}
		for (int i = 0;i<this.listaAristas.size();i++){
			MapPolygonImpl camino = new MapPolygonImpl(listaNodosMap.get(listaAristas.get(i).getElem1()).getElem2(),listaNodosMap.get(listaAristas.get(i).getElem2()).getElem2(),listaNodosMap.get(listaAristas.get(i).getElem1()).getElem2());
			this.mapa.addMapPolygon(camino);
			this.listaAristasMap.add(camino);
		}
		
	}
	
	private void guardarRed() {
		String nombre = JOptionPane.showInputDialog("Nombre del archivo: ");
		try{
			File file = new File(nombre);
			if (file.exists() && actual.compareTo(nombre)!=0){
				JOptionPane.showMessageDialog(ventana, "Ya existe una red con ese nombre.");
				throw new IllegalArgumentException("Ya existe una red con ese nombre.");
			}else{
				file.createNewFile();
			}			
			try{
				FileOutputStream fos =  new FileOutputStream(nombre);
				ObjectOutputStream out =  new ObjectOutputStream(fos);
				out.writeObject(grafo);
				out.writeObject(this.listaNodos);
				out.writeObject(this.listaAristas);
				out.writeObject(this.listaAristasCap);
				out.writeObject(this.listaNodosMap);
				out.close();		
				actual = nombre;
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private void cargarRed() {
		String nombre = JOptionPane.showInputDialog("Nombre del archivo: ");
		actual = nombre;
		try{
			File file = new File(nombre);		
			if (!file.exists()){
				JOptionPane.showMessageDialog(ventana, "No existe una red guardada con ese nombre.");
				throw new IllegalArgumentException("No existe una red guardada con ese nombre.");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			JOptionPane.showMessageDialog(ventana, "No existe una red guardada con ese nombre.");
		}
		try{
			FileInputStream fos =  new FileInputStream(nombre);
			ObjectInputStream in =  new ObjectInputStream(fos);
			vaciarRed();
			this.grafo = (Grafo) in.readObject();
			this.listaNodos.addAll((ArrayList<Tupla<String, Integer>>) in.readObject());
			this.listaAristas.addAll((ArrayList<Tupla<Integer, Integer>>) in.readObject());
			this.listaAristasCap.addAll((ArrayList<Integer>) in.readObject());
			this.listaNodosMap.addAll((ArrayList<Tupla<String,Coordinate>>) in.readObject());
			in.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		actualizarTabla();
		actualizarMapa();
	}
	
	private void nuevaRed() {
		actual = "";
		vaciarRed();
		actualizarTabla();
		actualizarMapa();
		this.labelFlujo.setText("");
	}

	private void vaciarRed() {
		this.grafo = new Grafo();
		this.listaNodos.clear();
		this.listaAristas.clear();
		this.listaAristasCap.clear();
		this.listaNodosMap.clear();
		this.listaAristasMap.clear();
		this.mapa.removeAllMapMarkers();
	}
}
