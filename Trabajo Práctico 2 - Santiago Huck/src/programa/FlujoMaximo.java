package programa;

import java.util.LinkedList; 
  
public class FlujoMaximo 
{ 
    
    public FlujoMaximo(){
    }

    boolean bfs(int grafoResidual[][], int origen, int destino, int pariente[], int cantVertices) 
    { 

        boolean visitado[] = new boolean[cantVertices]; 
        for(int i=0; i<cantVertices; ++i) 
            visitado[i]=false; 

        LinkedList<Integer> cola = new LinkedList<Integer>(); 
        cola.add(origen); 
        visitado[origen] = true; 
        pariente[origen]=-1; 
  
        while (cola.size()!=0) 
        { 
            int x = cola.poll(); 
  
            for (int y=0; y<cantVertices; y++) 
            { 
                if (visitado[y]==false && grafoResidual[x][y] > 0) 
                { 
                    cola.add(y); 
                    pariente[y] = x; 
                    visitado[y] = true; 
                } 
            } 
        } 
        
        return (visitado[destino] == true); 
    } 

    public int flujoMaximo(int grafoCapacidades[][], int origen, int destino) 
    { 
    	int cantVertices=grafoCapacidades.length;
    	if (cantVertices==0){
        	return 0;
        }
        int x, y; 

        int grafoResidual[][] = new int[cantVertices][cantVertices]; 
  
        for (x = 0; x < cantVertices; x++) 
            for (y = 0; y < cantVertices; y++) 
                grafoResidual[x][y] = grafoCapacidades[x][y]; 
  
        int pariente[] = new int[cantVertices]; 
  
        int flujoMaximo = 0; 

        while (bfs(grafoResidual, origen, destino, pariente, cantVertices)) 
        { 
            int flujoParcial = Integer.MAX_VALUE; 
            for (y=destino; y!=origen; y=pariente[y]) 
            { 
                x = pariente[y]; 
                flujoParcial = Math.min(flujoParcial, Math.min(grafoResidual[x][y], grafoResidual[x][x])); 
            } 
  
            for (y=destino; y != origen; y=pariente[y]) 
            { 
                x = pariente[y]; 
                grafoResidual[x][y] -= flujoParcial; 
                grafoResidual[y][x] += flujoParcial; 
            } 
  
            flujoMaximo += flujoParcial; 
        } 
   
        return flujoMaximo; 
    } 
   
}