package programa;

import static org.junit.Assert.*;

import org.junit.Test;

public class FlujoMaximoTest {
	
	@Test
	public void bfsVacia(){
		Grafo grafo = new Grafo();
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		int[] pariente = new int[grafo.vertices()];
		assertFalse(fm.bfs(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1, pariente,grafo.vertices()));
	}
	
	@Test
	public void bfsSimple(){
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Productor", 50);
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarArista(0, 1, 50);
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		int[] pariente = new int[grafo.vertices()];
		assertTrue(fm.bfs(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1, pariente,grafo.vertices()));
	}
	
	@Test
	public void grafoVacioFlujo(){
		Grafo grafo = new Grafo();
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		assertTrue(fm.flujoMaximo(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1)==0);
	}
	
	@Test
	public void unVerticeFlujo(){
		Grafo grafo = new Grafo();		
		grafo.agregarVertice("Productor", 50);
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		assertTrue(fm.flujoMaximo(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1)==0);
	}
	
	@Test
	public void dosVerticesFlujo(){
		Grafo grafo = new Grafo();		
		grafo.agregarVertice("Productor", 50);
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarArista(0, 1, 50);
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		assertTrue(fm.flujoMaximo(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1)==50);
	}
	
	@Test
	public void dosCaminosPosiblesFlujo(){
		Grafo grafo = new Grafo();		
		grafo.agregarVertice("Productor", 50);
		grafo.agregarVertice("De Paso", 30);
		grafo.agregarVertice("De Paso", 20);
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarArista(0, 1, 30);
		grafo.agregarArista(0, 2, 20);
		grafo.agregarArista(1, 3, 30);
		grafo.agregarArista(2, 3, 20);
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		assertTrue(fm.flujoMaximo(grafo.getCapacidades(),grafo.vertices()-2, grafo.vertices()-1)==50);
	}
		
	@Test
	public void capacidadAristaMenorVerticeFlujo(){
		Grafo grafo = new Grafo();		
		grafo.agregarVertice("Productor", 50);
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarArista(0, 1, 30);
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		assertTrue(fm.flujoMaximo(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1)==30);
	}
	
	@Test
	public void capacidadAristaMayorVerticeFlujo(){
		Grafo grafo = new Grafo();		
		grafo.agregarVertice("Productor", 50);
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarArista(0, 1, 80);
		grafo.prepararFM();
		FlujoMaximo fm = new FlujoMaximo();
		assertTrue(fm.flujoMaximo(grafo.getCapacidades(), grafo.vertices()-2, grafo.vertices()-1)==50);
	}

}
