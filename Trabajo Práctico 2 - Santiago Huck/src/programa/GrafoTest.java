package programa;

import static org.junit.Assert.*;

import org.junit.Test;

public class GrafoTest {

	@Test(expected = IllegalArgumentException.class)
	public void primeroNegativoTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarArista(-1, 3, 100);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void segundoNegativoTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarArista(2, -1, 100);
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesIgualesTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarArista(2, 2, 50);
	}

	@Test
	public void agregarVerticeTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		assertTrue( grafo.vertices()==1 );
	}	
	
	@Test
	public void agregarAristaTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 50);
		grafo.agregarArista(0, 1, 50);
		assertTrue( grafo.existeArista(0, 1) );
	}
	
	@Test
	public void aristaContrariaTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 50);
		grafo.agregarArista(0, 1, 50);
		assertFalse( grafo.existeArista(1, 0) );
	}

	@Test
	public void eliminarAristaTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 50);
		grafo.agregarArista(0, 1, 50);		
		grafo.eliminarArista(0, 1);
		assertFalse( grafo.existeArista(0, 1) );
	}
	
	@Test
	public void cantidadAristasTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 50);
		grafo.agregarArista(0, 1, 50);
		assertTrue( grafo.aristas()==1 );
		
	}

	@Test
	public void getTiposTest(){
		
		String[][] esperado = {{"1",""},{"","2"}};
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 50);	
		String[][] resultado = grafo.getTipos();
		assertTrue(resultado[0][0].equals(esperado[0][0]) && resultado[1][1].equals(esperado[1][1]));
		
	}
	
	@Test
	public void getCapacidadesTest(){
		
		int[][] esperado = {{50,0},{0,25}};
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 25);	
		int[][] resultado = grafo.getCapacidades();
		assertTrue(resultado[0][0]==esperado[0][0] && resultado[1][1]==esperado[1][1]);
		
	}
	
	@Test
	public void getAdyacenciaTest(){
		
		boolean[][] esperado = {{false,true},{false,false}};
		Grafo grafo = new Grafo();
		grafo.agregarVertice("1", 50);
		grafo.agregarVertice("2", 25);	
		grafo.agregarArista(0, 1, 25);
		boolean[][] resultado = grafo.getAdyacencia();
		assertTrue(resultado[0][1]==esperado[0][1] && resultado[1][0]==esperado[1][0]);
		
	}
	
	@Test
	public void prepararFyFTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarVertice("Productor", 50);
		grafo.agregarArista(0, 1, 50);
		grafo.prepararFM();
		assertTrue( grafo.vertices()==4 );
		
	}
	
	@Test
	public void doblePrepararFyFTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarVertice("Productor", 50);
		grafo.agregarArista(0, 1, 50);
		grafo.prepararFM();
		grafo.prepararFM();
		assertTrue( grafo.vertices()==4 );
		
	}
	
	@Test
	public void vacioPrepararFyFTest(){
		
		Grafo grafo = new Grafo();
		grafo.prepararFM();
		assertTrue( grafo.vertices()==2 );
		
	}
	
	@Test
	public void yaPreparadoTest(){
		
		Grafo grafo = new Grafo();
		grafo.agregarVertice("Consumidor", 50);
		grafo.agregarVertice("Productor", 50);
		grafo.agregarArista(0, 1, 50);
		grafo.prepararFM();
		assertTrue( grafo.yaPreparado());
		
	}

}
