package programa;

import java.io.Serializable;

public class Tupla<S,T> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private S elem1;
	private T elem2;
	
	public Tupla(S elem1, T elem2)
	{
		
		this.elem1 = elem1;
		this.elem2 = elem2;
		
	}
	
	public S getElem1()
	{
		
		return this.elem1;
		
	}
	
	public T getElem2()
	{
		
		return this.elem2;
		
	}
	
	public void setElem1(S nuevoElem1)
	{
		
		this.elem1 = nuevoElem1;
		
	}
	
	public void setElem2(T nuevoElem2)
	{
		
		this.elem2 = nuevoElem2;
		
	}
	
}
