package programa;

import java.io.Serializable;

public class Grafo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	boolean[][] matrizAdyacencia;
	String[][] matrizTipos;
	int[][] matrizCapacidades;
	private int vertices;
	private int aristas;
	
	public Grafo(){
		
		vertices = 0;
		aristas = 0;
		matrizAdyacencia = new boolean[vertices][vertices];
		matrizTipos = new String[vertices][vertices];
		matrizCapacidades = new int[vertices][vertices];
		
	}
	
	public int vertices(){
		
		return vertices;
	}
	
	public int aristas(){
		
		return aristas;
	}	
	
	public void agregarVertice(String tipo, Integer capacidad){
		
		this.vertices +=1;
		boolean[][] aux = matrizAdyacencia;
		matrizAdyacencia = new boolean[vertices][vertices];
		for (int i=0; i<aux.length;i++){
			for (int j=0; j<aux.length;j++){
				matrizAdyacencia[i][j]=aux[i][j];
			}
		}
		String[][] auxT = matrizTipos;
		matrizTipos = new String[vertices][vertices];
		for (int i=0; i<auxT.length;i++){
			for (int j=0; j<auxT.length;j++){
				matrizTipos[i][j]=auxT[i][j];
			}
		}
		int[][] auxC = matrizCapacidades;
		matrizCapacidades = new int[vertices][vertices];
		for (int i=0; i<auxC.length;i++){
			for (int j=0; j<auxC.length;j++){
				matrizCapacidades[i][j]=auxC[i][j];
			}
		}
		
		matrizTipos[vertices-1][vertices-1] = tipo;
		matrizCapacidades[vertices-1][vertices-1] = capacidad;
		
	}
		
	public void agregarArista(int i, int j, int capacidad){
		
		verificarArista(i, j);
		matrizAdyacencia[i][j] = true;
		matrizCapacidades[i][j] = capacidad;
		aristas +=1;
		
	}
	
	public void eliminarArista(int i, int j){
		
		verificarArista(i, j);
		matrizAdyacencia[i][j] = false;
		matrizCapacidades[i][j] = -1;
		aristas-=1;
		
	}
	
	public boolean existeArista(int i, int j){
		
		verificarArista(i, j);
		return matrizAdyacencia[i][j];
		
	}
	
	public boolean[][] getAdyacencia(){
		
		return this.matrizAdyacencia;
		
	}
		
	public String[][] getTipos(){
		
		return this.matrizTipos;
		
	}
	
	public int[][] getCapacidades(){
		
		for (int i=0; i<matrizCapacidades.length;i++){
			for (int j=0; j<matrizCapacidades.length;j++){
				if (matrizCapacidades[i][j]>=matrizCapacidades[i][i]){
					int x = matrizCapacidades[i][i];
					matrizCapacidades[i][j]=x;
				}
			}
		}
		return this.matrizCapacidades;
		
	}
	
	public void prepararFM(){
		
		boolean preparado = yaPreparado();
		if (preparado==false){
			agregarVertice("Origen",0);
		}		
		int cont = 0;
		for (int i=0; i<vertices();i++){
			if (matrizTipos[i][i].equals("Productor") && !existeArista(vertices()-1,i)){
				agregarArista(vertices()-1,i,matrizCapacidades[i][i]);	
				cont += matrizCapacidades[i][i];
			}
		}
		matrizCapacidades[vertices()-1][vertices()-1] = cont;
		if (preparado==false){
			agregarVertice("Destino",0);
		}		
		cont = 0;
		for (int i=0; i<vertices();i++){
			if (matrizTipos[i][i].equals("Consumidor") && !existeArista(i,vertices()-1)){
				agregarArista(i,vertices()-1,matrizCapacidades[i][i]);	
				cont += matrizCapacidades[i][i];
			}
		}
		matrizCapacidades[vertices()-1][vertices()-1] = cont;	
		
	}
	

	public boolean yaPreparado(){
		
		boolean aux = false;
		for (int i=0; i<vertices();i++){
			if (matrizTipos[i][i].equals("Origen") || matrizTipos[i][i].equals("Destino")){
				aux = true;
			}
		}
		return aux;
		
	}


	private void verificarArista(int i, int j){
		
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
	}

	private void verificarVertice(int i){
		
		if( i < 0 || i >= vertices() )
			throw new IllegalArgumentException("El vertice " + i + "no existe!");
		
	}

	private void verificarDistintos(int i, int j){
		
		if( i == j )
			throw new IllegalArgumentException("No se permiten aristas "
					+ "entre un mismo vertice! (" + i + ")");
		
	}

}
